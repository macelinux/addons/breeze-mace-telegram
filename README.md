# Breeze-Mace Theme for Telegram 

## Description

A theme to use with Telegram matching the Mace color schemes. 

## Installation

You can download the source here or visit the telegram share at this link:  
https://t.me/addtheme/BreeezeMaceDark

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)
